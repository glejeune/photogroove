import * as path from "path";
import elm from "rollup-plugin-elm";
import { terser } from "rollup-plugin-terser";
import resolve from "@rollup/plugin-node-resolve";
import postcss from "rollup-plugin-postcss";

export default [
  {
    input: "src/main.js",
    output: {
      file: `dist/bundle_main.js`,
      format: "iife"
    },
    plugins: [
      elm({
        exclude: "elm_stuff/**",
        compiler: {
          optimize: true,
          debug: false,
          pathToElm: path.resolve(__dirname, "node_modules/elm/bin/elm")
        }
      }),
      terser()
    ]
  },
  {
    input: "src/range-slider.js",
    context: "window",
    output: {
      file: "dist/range-slider.js",
      format: "iife"
    },
    plugins: [
      resolve(),
      postcss({
        plugins: []
      }),
      terser()
    ]
  }
];
